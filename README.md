# python security camera

The code is based on opencv. It detects motion in webcam feed, makes screenshots and uploads them on the specified FTP folder.
- Encode the FTP details in **security_access.py** (domain, user and password)
- Test camera with **cam_test.p**
- Run **security_cam.py**