import cv2

CAMERA = 0

video = cv2.VideoCapture(CAMERA)

while True:
	try:
		check, frame = video.read() 
		cv2.imshow("Color Frame", frame)
	except:
		pass
	key = cv2.waitKey(1) 
	if key == ord('q'):
		break

video.release()

cv2.destroyAllWindows()
