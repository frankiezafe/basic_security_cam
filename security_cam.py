'''
sudo apt install python3-opencv
pour lancer un programme dans le server X (interface graphique)
$ export DISPLAY=:0
source: https://superuser.com/questions/1035787/open-a-movie-with-vlc-from-command-line-via-ssh
'''
# sudo -H python3 -m pip install opencv-python
# sudo apt install python3-opencv
# https://www.geeksforgeeks.org/webcam-motion-detector-python/
# Pyhton program to implement 
# WebCam Motion Detector 

import cv2, time, os, ftplib, security_access
from datetime import datetime
from ftplib import FTP

CAMERA = 0
BLUR = 21
THRESHOLD = 10
MIN_AREA = 50
REFRESH_BACK_RATE = 1
SHOW_IMAGES = True
ANALYSIS_RESOLUTION = (320,240)
FOLDER = "screenshots"
QUIET = True

static_back = None

video = cv2.VideoCapture(CAMERA)

frame_count = 0
previous_ts = None
counter_ts = 0

if not os.path.isdir( FOLDER ):
	os.mkdir( FOLDER )

def unik_name():
	
	global previous_ts
	global counter_ts
	
	now = datetime.now()
	t = str( now.strftime( "%Y-%m-%d_%H.%M.%S_" ) )
	if t == previous_ts:
		counter_ts += 1
	else:
		previous_ts = t
		counter_ts = 0
	cs = str( counter_ts )
	while( len(cs) < 3 ):
		cs = '0' + cs
	print( t, ':', cs )
	return  t + "cam" + str(CAMERA) + "_" + cs + ".jpg"

def ftp_upload( path, name ):
	try:
		ftp = FTP( security_access.HOST )
		ftp.login( security_access.USER, security_access.PSSWRD )
		ftp.cwd( FOLDER )
		file = open(path,'rb')
		ftp.storbinary('STOR ' + name, file)
		file.close()
		ftp.quit()
	except Exception as e:
		print( "ftp crash!", e )
		pass

while True: 
	try:
		check, frame = video.read() 
		frame_height, frame_width, frame_channels = frame.shape
		frame_scale = [ frame_width * 1.0 / ANALYSIS_RESOLUTION[0], frame_height * 1.0 / ANALYSIS_RESOLUTION[1] ]
	
		low_res = cv2.resize(frame, ANALYSIS_RESOLUTION)
	
		frame_count += 1
	
		gray = cv2.cvtColor(low_res, cv2.COLOR_BGR2GRAY) 
		gray = cv2.GaussianBlur(gray, (BLUR, BLUR), 0) 
		if static_back is None: 
			static_back = gray
			continue
	
		diff_frame = cv2.absdiff(static_back, gray) 
		thresh_frame = cv2.threshold(diff_frame, THRESHOLD, 255, cv2.THRESH_BINARY)[1] 
		thresh_frame = cv2.dilate(thresh_frame, None, iterations = 2) 

		cnts = None
		cnts, _ = cv2.findContours( thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE )
	
		if cnts != None:
			contour_num = 0
			for contour in cnts: 
				if cv2.contourArea(contour) < MIN_AREA: 
					continue
				(x, y, w, h) = cv2.boundingRect(contour) 
				cv2.rectangle( low_res, (x, y), (x + w, y + h), (0, 255, 0), 3)
				contour_num += 1
			if len( cnts ) > 0:
				fname = unik_name()
				print( fname )
				fpath = os.path.join( FOLDER, fname )
				cv2.imwrite( fpath, frame )
				ftp_upload( fpath, fname )
	
		if SHOW_IMAGES:
			if not QUIET:
				cv2.imshow("Gray Frame", gray) 
				cv2.imshow("Difference Frame", diff_frame) 
				cv2.imshow("Threshold Frame", thresh_frame) 
			cv2.imshow("Color Frame", low_res)
	
		if REFRESH_BACK_RATE != -1 and frame_count % REFRESH_BACK_RATE == 0: 
			static_back = gray
	except:
		pass
	
	key = cv2.waitKey(1) 
	if key == ord('q'):
		break

video.release()

cv2.destroyAllWindows()
